import { CourseRegistration } from './../classes/course-registration';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Course } from '../classes/course';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root',
})
export class CourseService {
  private baseUrl: string;
  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080';
  }

  public getAllCourses(id: number): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.baseUrl}/getAllCourses/${id}`);
  }
  public _getAllCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(`${this.baseUrl}/allCourses`);
  }

  public getCourse(id: number): Observable<Course> {
    return this.http.get<Course>(`${this.baseUrl}/course/${id}`);
  }
}
