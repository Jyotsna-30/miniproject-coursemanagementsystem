import { CourseRegistration } from './../classes/course-registration';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Course } from '../classes/course';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080/admin';
  }
  public saveCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(`${this.baseUrl}/saveCourse`, course);
  }

  public editCourse(course: Course): Observable<Course> {
    return this.http.put<Course>(`${this.baseUrl}/editCourse`, course);
  }

  public deleteCourse(id: number) {
    return this.http.delete(`${this.baseUrl}/deleteCourse/${id}`);
  }

  public assignCourse(
    courseRegistration: CourseRegistration
  ): Observable<CourseRegistration> {
    console.log(courseRegistration);
    return this.http.put<CourseRegistration>(
      `${this.baseUrl}/assignCourse`,
      courseRegistration
    );
  }

  public getAllEnrolledCourses(): Observable<CourseRegistration[]> {
    return this.http.get<CourseRegistration[]>(
      `${this.baseUrl}/getEnrolledCourses`
    );
  }

  public getAllAssignedCourses(): Observable<CourseRegistration[]> {
    return this.http.get<CourseRegistration[]>(
      `${this.baseUrl}/getAssignedCourses`
    );
  }

  public deleteAssignedCourse(id: number) {
    return this.http.delete(`${this.baseUrl}/deleteAssignedCourse/${id}`);
  }

  public assignCoursetoUser(
    courseName: string,
    userName: string,
    remarks: string
  ): Observable<CourseRegistration> {
    let params = new HttpParams();
    params = params.set('courseName', courseName);
    params = params.set('userName', userName);
    params = params.set('remarks', remarks);
    return this.http.get<CourseRegistration>(
      `${this.baseUrl}/assignCourseuser`,
      {
        params: params,
      }
    );
  }
}
