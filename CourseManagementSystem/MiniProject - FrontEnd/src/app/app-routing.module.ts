import { CoursesInfoComponent } from './admin/courses-info/courses-info.component';
import { CourseRequestsComponent } from './admin/course-requests/course-requests.component';
import { EditCourseComponent } from './admin/edit-course/edit-course.component';
import { MyCoursesComponent } from './user/my-courses/my-courses.component';

import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRegistrationComponent } from './user/user-registration/user-registration.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserComponent } from './user/user/user.component';
import { AdminComponent } from './admin/admin/admin.component';
import { SaveCourseComponent } from './admin/save-course/save-course.component';
import { AllCoursesComponent } from './user/all-courses/all-courses.component';
import { CoursesComponent } from './admin/courses/courses.component';
import { AuthGuard } from './auth.guard';
import { AdminGuard } from './admin.guard';

const routes: Routes = [
  { path: 'registeruser', component: UserRegistrationComponent },
  { path: 'registeruser/userlogin', component: UserLoginComponent },
  { path: 'userlogin', component: UserLoginComponent },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },

  {
    path: 'saveCourse',
    component: SaveCourseComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'getallcourses',
    component: AllCoursesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'allcourses',
    component: CoursesComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'mycourses',
    component: MyCoursesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'editcourse/:id',
    component: EditCourseComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'courseRequests',
    component: CourseRequestsComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'coursesInfo',
    component: CoursesInfoComponent,
    canActivate: [AdminGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
