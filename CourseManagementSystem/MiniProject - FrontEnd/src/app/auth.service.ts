import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  isUserLoggedIn() {
    if (localStorage.getItem('userId') != null) return true;
    else return false;
  }

  isAdminLoggedIn() {
    if (localStorage.getItem('admin') != null) {
      return true;
    } else {
      return false;
    }
  }
}
