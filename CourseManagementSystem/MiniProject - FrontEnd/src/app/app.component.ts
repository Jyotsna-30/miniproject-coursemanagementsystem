import { Component } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormsModule,
  NgForm,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Miniproject';
  constructor(private router: Router, private service: AuthService) {}
  ngOnInit(): void {
    //this.router.navigate(['/userlogin']);
  }
  isLoggedIn() {
    if (this.service.isUserLoggedIn() || this.service.isAdminLoggedIn())
      return true;
    else return false;
  }
}
