import { User } from './user';
import { Course } from './course';

export class CourseRegistration {
  id: number;
  user: User;
  course: Course;
  assignedDate: Date;
  status: String;
  completionStatus: String;
  remarks: String;
}
