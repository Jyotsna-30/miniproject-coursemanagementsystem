export class User {
  userId: number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  emailId: string;
  password: string;
  disabled: boolean;
}
