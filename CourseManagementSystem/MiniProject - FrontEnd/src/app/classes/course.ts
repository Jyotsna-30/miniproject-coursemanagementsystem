import { User } from './user';

export class Course {
  courseId: number;
  courseName: string;
  description: string;
  link: string;
}
