import { AdminService } from 'src/app/services/admin.service';
import { CourseService } from 'src/app/services/course.service';
import { Course } from 'src/app/classes/course';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css'],
})
export class EditCourseComponent implements OnInit {
  course: Course = new Course();
  _course: Course = new Course();
  id: number;
  editCourseForm = new FormGroup({
    courseId: new FormControl(''),
    courseName: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    link: new FormControl('', [Validators.required]),
  });

  constructor(
    private _route: ActivatedRoute,
    private courseService: CourseService,
    private adminService: AdminService,
    private router: Router
  ) {
    this._route.paramMap.subscribe((parameterMap) => {
      this.id = +parameterMap.get('id');
    });
    this.courseService.getCourse(this.id).subscribe((data) => {
      this._course = data;
      this.editCourseForm.setValue(this._course);
      console.log(data);
    });
  }

  ngOnInit(): void {}

  editCourse() {
    console.log('Edit course Form');
    this.course = this.editCourseForm.value;
    this.course.courseId = this._course.courseId;
    this.adminService.editCourse(this.course).subscribe(
      (data) => {
        this.router.navigate(['/admin']);
        console.log('updated successfully');
      },
      (error) => {
        console.log('Updation failed');
      }
    );
  }
}
