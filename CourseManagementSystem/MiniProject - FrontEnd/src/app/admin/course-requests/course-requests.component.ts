import { AdminService } from 'src/app/services/admin.service';
import { Component, OnInit } from '@angular/core';
import { CourseRegistration } from '../../classes/course-registration';

@Component({
  selector: 'app-course-requests',
  templateUrl: './course-requests.component.html',
  styleUrls: ['./course-requests.component.css'],
})
export class CourseRequestsComponent implements OnInit {
  courses: CourseRegistration[];
  page: number = 1;
  totalLength: number;
  searchValue: string;
  empty: boolean;
  constructor(private adminService: AdminService) {}

  ngOnInit(): void {
    this.adminService.getAllEnrolledCourses().subscribe(
      (data) => {
        this.courses = data;
        this.totalLength = this.courses.length;
        if (this.totalLength == 0) {
          this.empty = true;
        }
        console.log(this.courses);
      },
      (error) => {
        console.log('Error in getting allEnrolledCourses');
      }
    );
  }
  assignCourse(course: CourseRegistration) {
    console.log('Assigning course');
    this.adminService.assignCourse(course).subscribe(
      (data) => {
        console.log(course);
        console.log('Assigned successfully');
        this.ngOnInit();
      },
      (error) => {
        console.log('error in assigning the course');
      }
    );
  }
}
