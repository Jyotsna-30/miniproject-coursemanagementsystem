import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
})
export class UserLoginComponent implements OnInit {
  user: User = new User();

  userLoginForm = new FormGroup({
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  userLogin() {
    console.log('User Login');
    this.user = this.userLoginForm.value;
    this.userService.userLogin(this.user).subscribe(
      (data) => {
        if (
          this.userLoginForm.controls['emailId'].value == 'admin@g.com' &&
          this.userLoginForm.controls['password'].value == 'admin'
        ) {
          localStorage.setItem('admin', this.user.emailId);
          this.router.navigate(['/admin']);
        } else {
          localStorage.setItem('userId', this.user.emailId);
          console.log(localStorage.getItem('userId'));

          this.router.navigate(['/user']);
        }
        console.log('Logged in successfully');
      },
      (error) => {
        console.log('Error in Logging in');
        alert('Bad Credentials');
      }
    );
  }
}
