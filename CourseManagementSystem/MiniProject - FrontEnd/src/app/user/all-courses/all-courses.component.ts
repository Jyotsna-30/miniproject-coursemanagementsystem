import { User } from 'src/app/classes/user';
import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/services/course.service';
import { Course } from 'src/app/classes/course';
import { CourseRegistration } from 'src/app/classes/course-registration';

@Component({
  selector: 'app-all-courses',
  templateUrl: './all-courses.component.html',
  styleUrls: ['./all-courses.component.css'],
})
export class AllCoursesComponent implements OnInit {
  constructor(
    private courseService: CourseService,
    private userService: UserService
  ) {}
  courses: Course[];
  private id: string;
  private user: User;
  searchValue: string;
  page: number = 1;
  totalLength: number;
  private registeredCourse: CourseRegistration;
  ngOnInit(): void {
    this.id = this.id = localStorage.getItem('userId');
    this.userService.getUser(this.id).subscribe(
      (data) => {
        this.user = data;
        this.courseService.getAllCourses(this.user.userId).subscribe(
          (data) => {
            this.courses = data;
            this.totalLength = this.courses.length;
          },
          (error) => {
            console.log('Error in getting courses');
          }
        );
        console.log(data);
      },
      (error) => console.log('error 1')
    );
  }

  enrollCourse(courseName) {
    this.id = localStorage.getItem('userId');
    this.userService.getUser(this.id).subscribe(
      (data) => {
        this.user = data;
        this.assignCourse(courseName);
      },
      (error) => console.log('error 1')
    );
  }

  assignCourse(courseName) {
    console.log(courseName);
    this.userService.enrollCourse(this.user, courseName).subscribe(
      (data) => {
        console.log('finished');
        this.ngOnInit();
        this.registeredCourse = data;
      },
      (error) => {
        console.log('error ');
      }
    );
  }
}
