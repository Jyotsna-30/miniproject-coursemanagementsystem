import { CourseRegistration } from './../../classes/course-registration';
import { User } from 'src/app/classes/user';
import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/classes/course';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.css'],
})
export class MyCoursesComponent implements OnInit {
  private userId: string;
  page: number = 1;
  totalLength: number;
  searchValue: string;
  empty: boolean;
  courseRegistration: CourseRegistration[];
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userId = localStorage.getItem('userId');
    this.userService.getAllCourses(this.userId).subscribe(
      (data) => {
        this.courseRegistration = data;
        this.totalLength = this.courseRegistration.length;
        if (this.totalLength == 0) {
          this.empty = true;
        }
        console.log(this.courseRegistration);
        console.log('My courses retrived');
      },
      (error) => console.log('Error in fetching the values')
    );
  }
  changeStatus(courseReg: CourseRegistration) {
    // this.courseRegistration.com;
    courseReg.completionStatus = 'Completed';
    this.userService.changeStatus(courseReg).subscribe((data) => {
      console.log('changed');
    });
  }
}
