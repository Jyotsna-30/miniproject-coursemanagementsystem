package com.miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.model.Course;

public interface CourseRepository extends JpaRepository<Course, Integer> {

	public Course findCourseByCourseName(String courseName);

	public Course findCourseByCourseId(int id);

}
