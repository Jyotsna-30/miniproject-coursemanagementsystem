package com.miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findUserByEmailId(String emailId);

	public User findUserByEmailIdAndPassword(String emailId, String password);

	public User findUserByFirstName(String firstName);
}
