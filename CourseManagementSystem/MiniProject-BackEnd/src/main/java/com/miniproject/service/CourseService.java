package com.miniproject.service;

import java.util.List;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.model.User;

public interface CourseService {
	public Course saveCourse(Course course);

	public Course getCourseByCourseName(String courseName);

	public Course getCourseById(int id);

	public List<Course> getAllCourses();

	public List<CourseRegistration> getAllCourses(int userId);

	public List<CourseRegistration> getAllEnrolledCoursesOfUser(int id);

	public CourseRegistration enrollCourse(CourseRegistration courseRegistration);

	public CourseRegistration getRegisteredCourse(Course course);

	public CourseRegistration getRegisteredCourseById(int Id);

	public CourseRegistration getRegisteredCourseByUserAndCourse(User user, Course course);

	public void deleteCourse(Course course);

	public void deleteRegisteredCourse(CourseRegistration courseRegistration);

	public CourseRegistration assignCourse(CourseRegistration courseRegistration);

	public CourseRegistration changeSatus(CourseRegistration course);

	public List<CourseRegistration> getAllAssignedCourses();

	public List<CourseRegistration> getAllEnrolledCourses();

	public CourseRegistration assignCourseToUser(String courseName, String userName, String remarks);
}
