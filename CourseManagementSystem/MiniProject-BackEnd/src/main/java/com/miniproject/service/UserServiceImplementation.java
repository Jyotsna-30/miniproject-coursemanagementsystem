package com.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miniproject.model.User;
import com.miniproject.repository.UserRepository;

@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public User registerUser(User user) {
		return userRepository.save(user);

	}

	@Override
	public User getUserByEmailId(String emailId) {
		return userRepository.findUserByEmailId(emailId);
	}

	@Override
	public User getUserByEmailIdAndPassword(String emailId, String password) {
		return userRepository.findUserByEmailIdAndPassword(emailId, password);
	}

}
