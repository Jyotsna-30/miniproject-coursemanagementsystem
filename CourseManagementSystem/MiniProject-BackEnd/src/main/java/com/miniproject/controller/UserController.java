package com.miniproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.model.User;
import com.miniproject.service.CourseService;
import com.miniproject.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	CourseService courseService;

	@PostMapping("/user/register")
	public ResponseEntity<User> registerUser(@RequestBody User user) throws Exception {

		String tempEmailId = user.getEmailId();
		if (tempEmailId != null) {
			User userobject = userService.getUserByEmailId(tempEmailId);
			if (userobject != null) {
				throw new Exception("user with this mailId already exists");
			}
		}
		return new ResponseEntity<>(userService.registerUser(user), HttpStatus.CREATED);
	}

	@PostMapping("/user/login")
	public ResponseEntity<User> userLogin(@RequestBody User user) throws Exception {
		String tempmailId = user.getEmailId();
		String temppassword = user.getPassword();
		User _user = null;
		if (tempmailId != null && temppassword != null) {
			_user = userService.getUserByEmailIdAndPassword(tempmailId, temppassword);
		}
		if (_user == null) {
			throw new Exception("Bad Credentials");
		}

		return new ResponseEntity<>(_user, HttpStatus.CREATED);
	}

	@GetMapping("/user/userinfo")
	public ResponseEntity<User> getUser(@RequestParam("emailId") String emailId) {
		return new ResponseEntity<User>(userService.getUserByEmailId(emailId), HttpStatus.OK);
	}

	@PostMapping("/user/enrollCourse")
	public ResponseEntity<CourseRegistration> enrollCourse(@RequestBody User user,
			@RequestParam("courseName") String courseName) {
		Course course = courseService.getCourseByCourseName(courseName);
		CourseRegistration courseRegistration = courseService.getRegisteredCourseByUserAndCourse(user, course);
		if (courseRegistration == null)
			courseRegistration = new CourseRegistration(user, course);

		return new ResponseEntity<CourseRegistration>(courseService.enrollCourse(courseRegistration), HttpStatus.OK);
	}

	@PostMapping("/user/getAllCourses")
	public ResponseEntity<List<CourseRegistration>> getAllCourses(@RequestBody String emailId) {
		User user = userService.getUserByEmailId(emailId);
		return new ResponseEntity<List<CourseRegistration>>(courseService.getAllCourses(user.getUserId()),
				HttpStatus.OK);
	}

	@PutMapping("/user/changeStatus")
	public ResponseEntity<CourseRegistration> changeStatus(@RequestBody CourseRegistration course) {
		course.setCompletionStatus("Completed");

		return new ResponseEntity<CourseRegistration>(courseService.changeSatus(course), HttpStatus.OK);
	}

}
